// Javascript Operators

// Arithmetic Operators

let x = 1397;
let y= 7831;

let sum = x + y;
console.log("result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators (=) - assigns the value of the right operand to a variable

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment Operator: " + assignmentNumber);

// Shorthand
assignmentNumber += 2;
console.log("Result of addition assignment Operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);
assignmentNumber *= 2;
console.log("Result of multiplicatiom assignment operator: " + assignmentNumber);
assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);


// Multiple Operators and Parentheses
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator: " + mdas);

// Using Parentheses
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operator: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment & Decrement Operators (++/--)
// Increment
let z = 1;

//pre-increment(++z)
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//post-increment(z++)
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);


// Decrement (--)
//pre-increment(--z)
decrement = --z;
console.log("Result of pre-decrement" + decrement);
console.log("Result of pre-decrement" + z);

//post-increment(z--)
decrement = z--;
console.log("Result of post-decrement" + decrement);
console.log("Result of post-decrement" + z);

// Type Coercion
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

// Comparison Operators
let juan = 'juan';

// Equality Operator (==)
console.log(1 == 1); /*true*/
console.log(1 == 2); /*false*/
console.log(1 == '1'); /*true*/
console.log(0 == false); /*true*/
console.log('juan' == juan); /*true*/

// Inequality operator (!=)
console.log(1 != 1); /*fales*/
console.log(1 != 2); /*true*/
console.log(1 != '1'); /*false*/
console.log(0 != false); /*false*/
console.log('juan' != juan); /*false*/

//Strict Equality Operator (===)
console.log(1 === 1); /*true*/
console.log(1 === 2); /*false*/
console.log(1 === '1'); /*false*/
console.log(0 === false); /*false*/
console.log('juan' === juan); /*true*/

//Strict Inequality Operator (!==)
console.log(1 !== 1); /*false*/
console.log(1 !== 2); /*true*/
console.log(1 !== '1'); /*true*/
console.log(0 !== false); /*true*/
console.log('juan' !== juan); /*false*/

// Relational Operators (<>=)
let a = 50;
let b = 65;

// Greater than (>)
let isGreaterThan = a > b;
// Less Than (<)
let isLessThan = a < b;
// Greater Than or Equal (>=)
let isGTorEqual = a >= b;
// Less Than or Equal (>=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

//Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical And Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical Or Opperator (||)
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet)

// Logical Not Operator (!)
// Return the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);